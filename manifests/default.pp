#include ::java
java::oracle { 'jdk8' :
  ensure  => 'present',
  version => '8',
  java_se => 'jdk',
}
->
class { 'elasticsearch': }
elasticsearch::instance { 'es-01': 
  config => { 'network.host' => '0.0.0.0' },
  datadir => '/vagrant_data/es-01',
  jvm_options => [
    '-Xms128m',
    '-Xmx128m'
  ]
}
->
class { 'kibana': 
  ensure => latest,
  config => {
    'server.host' => '0.0.0.0',
    'server.port' => '5601',
    'elasticsearch.url' => 'http://localhost:9200',
  },
}