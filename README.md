Dev Elasticsearch machine for local development. Box contains both elasticsearch abnd Kibana, provisioned using (you guessed right) - Puppet.
After the box is running you can browse to http://127.0.0.1:5601 for Kibana and ES on http://127.0.0.1:9200

---

## Running Machine


1. Install VirtualBox.
2. Install Vagrant.
3. Execute ```vagrant plugin install vagrant-vbguest``` (you should execute it only once, as part of Vagrant installation).
4. Clone the repo (and cd).
5. mkdir 'data' folder (leave it empty)
6. Execute ```vagrant up``` (should take a couple of minutes)
7. Try the links in your browser.
8. You can ssh into the machine using ```vagrant ssh``` command (```logout``` to exit).
9. ```vagrant destory``` will kill the machine (```vagrant suspend``` to sleep, ```vagrant halt``` for graceful shutdown).